(defproject org.clojars.wactbprot/vl-data-insert "0.1.0"
  :description "Functions to insert vaclab style data in vaclab style database documents."
  :url "https://codeberg.org/wactbprot/vl-data-insert"
  :license {:name "EPL-2.0 OR GPL-2.0-or-later WITH Classpath-exception-2.0"
            :url "https://www.eclipse.org/legal/epl-2.0/"}
  :dependencies [[org.clojure/clojure "1.10.0"]]
  :repl-options {:init-ns vl-data-insert.core})
